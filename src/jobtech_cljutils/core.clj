(ns jobtech-cljutils.core)

(defmacro map-from-symbols [& symbols]
  `(hash-map ~@(for [s symbols x [(-> s name keyword) s]] x)))

(defn flatten-with-fn [expand-element input]
  (loop [stack (list input)
         output []]
    (if (empty? stack)
      output
      (let [[f & stack] stack]
        (if-let [expansion (expand-element f)]
          (recur (into stack (reverse expansion)) output)
          (recur stack (conj output f)))))))

(defn find-paths [predicate data]
  (loop [[tos & stack] [[[] data]]
         result []]
    (if (nil? tos)
      result
      (let [[path data] tos
            result (if (predicate data)
                     (conj result path)
                     result)]
        (cond
          (map? data) (recur (into stack
                                   (map (fn [[k v]] [(conj path k) v]))
                                   data)
                             result)
          (sequential? data) (recur (into stack
                                          (map-indexed (fn [i v] [(conj path i) v]))
                                          data)
                                    result)
          :else (recur stack result))))))

(defn check-mapped [x]
  (assert (or (nil? x) (and (vector? x) (= 2 (count x)))))
  x)

(defn pop-n [stack n]
  [(take n stack) (drop n stack)])

(defn walk-and-map
  "Traverses a datastructure `src` and optionally maps elements in the datastructure. `mapper` is a function that either takes no arguments and returns an initial state, or takes one element currently being visited and returns a vector of the new state and the mapped element. Note that it will not descent recursively into elements that it maps."
  [src mapper]
  {:pre [(fn? mapper)]}
  (loop [stack (list [:input src])
         result '()
         mapper-state (mapper)]
    (if (empty? stack)
      (do (assert (= 1 (count result)))
          [(first result) mapper-state])
      (let [[[op x ks] & stack] stack
            expand-coll (fn [coll-type ks elements]
                          (into (conj stack [coll-type (count elements) ks])
                                (map (fn [x] [:input x]))
                                elements))]
        (if (= :input op)
          (if-let [[mapper-state mapped] (check-mapped (mapper mapper-state x))]
            (recur stack (conj result mapped) mapper-state)
            (cond
              (map? x) (recur (expand-coll :map (keys x) (vals x)) result mapper-state)
              (vector? x) (recur (expand-coll :vec nil x) result mapper-state)
              (set? x) (recur (expand-coll :set nil x) result mapper-state)
              (sequential? x) (recur (expand-coll :seq nil x) result mapper-state)
              :else (recur stack (conj result x) mapper-state)))
          (recur stack
                 (let [[vs result] (pop-n result x)]
                   (conj result
                         (case op
                           :map (zipmap ks vs)
                           :set (set vs)
                           :vec (vec vs)
                           :seq (seq vs))))
                 mapper-state))))))
