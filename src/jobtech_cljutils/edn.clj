(ns jobtech-cljutils.edn
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))

;; https://stackoverflow.com/questions/27064337/write-large-data-structures-as-edn-to-disk-in-clojure

(defn write-edn [file data]
  (with-open [w (io/writer file)]
    (binding [*print-length* false
              *out* w]
      (pr data))))

(defn read-edn [file]
  (with-open [r (io/reader file)]
    (edn/read (java.io.PushbackReader. r))))
