(ns jobtech-cljutils.io
  (:require [clojure.java.io :as io])
  (:import [java.nio.file Files]
           [java.io File]
           [java.nio.file.attribute FileAttribute]))

(defn file? [x]
  (instance? File x))

(defn create-temp-directory [prefix]
  (Files/createTempDirectory prefix (make-array FileAttribute 0)))

(defn numbered-file-spitter [output-file-format]
  (let [output-file-format (io/file output-file-format)
        parent (.getParent output-file-format)
        name (.getName output-file-format)
        counter (atom 0)]
    (fn [data]
      (let [outfile (io/file parent (format name (swap! counter inc)))]
        (io/make-parents outfile)
        (spit outfile data)
        outfile))))
