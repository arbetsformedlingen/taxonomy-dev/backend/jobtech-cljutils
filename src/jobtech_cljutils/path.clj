(ns jobtech-cljutils.path
  (:import [java.nio.file Paths Files LinkOption Path]
           [java.io File])
  (:require [jobtech-cljutils.io :refer [file?]]))

(defn path? [x]
  (instance? Path x))

(defn path-exists? [p]
  {:pre [(path? p)]}
  (Files/exists p (into-array LinkOption [])))

(defn normalize [x]
  (cond
    (path? x) x
    (string? x) (Paths/get x (make-array String 0))
    (file? x) (.toPath x)
    :else (throw (ex-info "Cannot make path from value" {:value x}))))

(defn- resolve-sub [^Path dst x]
  (cond
    (string? x) (.resolve dst ^String x)
    (path? x) (.resolve dst ^Path x)))


;;(Paths/get first (into-array String more))

(defn path [first & more]
  (reduce resolve-sub
          (normalize first)
          more))


(defn absolute-path? [x]
  (and (path? x)
       (.isAbsolute ^Path x)))

(defn relative-path? [x]
  (and (path? x)
       (not (.isAbsolute x))))
