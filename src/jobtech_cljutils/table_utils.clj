(ns jobtech-cljutils.table-utils)

(defn nested-rows? [x]
  (and (sequential? x)
       (every? sequential? x)))

(defn- seqind [x]
  (if (sequential? x)
    (seq x)
    (list x)))

(defn flat-table-map
  ([x] (flat-table-map x identity))
  ([x cellf]
   (loop [[[type data] & ros] (list [:subtable x])
          output '()]
     (case type
       nil (do (assert (= 1 (count output)))
               (first output))
       :subtable (cond
                   (nested-rows? data) (let [flat-cells
                                             (for [[i row] (map-indexed vector data)
                                                   [j cell] (map-indexed vector row)]
                                               [[i j] cell])
                                             inds (map first flat-cells)]
                                         (recur (into (conj ros [:make inds])
                                                      (map (fn [[_ x]] [:subtable x]))
                                                      flat-cells)
                                                output))
                   (map? data) (recur ros
                                      (conj output
                                            (into {}
                                                  (map (fn [[[i j] x]]
                                                         [[(seqind i)
                                                           (seqind j)] x]))
                                                  data)))
                   :else (recur ros (conj output {['() '()] (cellf data)})))
       :make (let [n (count data)
                   [args output] (split-at n output)]
               (recur ros (conj output (into {} (for [[[outer-i outer-j] arg]
                                                      (map vector data args)
                                                      [[inner-i inner-j] x] arg]
                                                  [[(conj inner-i outer-i)
                                                    (conj inner-j outer-j)] x])))))))))

(defn flat-nested-vectors
  ([x] (flat-nested-vectors x identity))
  ([x cellf]
   (let [x (flat-table-map x)
         ks (keys x)
         index-set #(sort (into #{} (map (comp vec %)) ks))
         row-inds (index-set first)
         col-inds (index-set second)]
     (into []
           (map (fn [i]
                  (into []
                        (map (fn [j] (cellf (get x [i j]))))
                        col-inds)))
           row-inds))))

(defn table-row [& values]
  [(vec values)])

(defn table-column [& values]
  (mapv vector values))

(defn hcat [tables]
  (flat-nested-vectors [(mapv flat-nested-vectors tables)]))

(defn vcat [tables]
  (flat-nested-vectors (mapv (comp vector flat-nested-vectors) tables)))

(defn- build-index [spec]
  (loop [[[path f] & stack] (list [[] spec])
         out {}]
    (cond
      (nil? path) out
      (vector? f) (recur (into stack (for [x (reverse f)]
                                       [path x])) out)
      (map? f) (let [path (conj path (:key f))]
                 (recur (conj stack [path (vec (:children f))])
                        (assoc out path (count out))))
      :else (assert false))))

(defn matrix [row-spec col-spec cells]
  (let [row-map (build-index row-spec)
        col-map (build-index col-spec)]
    (into {}
          (map (fn [[[row col] v]] [[(row-map row) (col-map col)] v]))
          cells)))

