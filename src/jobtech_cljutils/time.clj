(ns jobtech-cljutils.time
  (:import [java.time Instant Duration]))

(defn now []
  (Instant/now))

(defn instant? [x]
  (instance? Instant x))

(defn duration? [x]
  (instance? Duration x))

(defn parse-instant [x]
  (Instant/parse x))

(defn max-instant [a b]
  (if (.isBefore a b) b a))

(defn min-instant [a b]
  (if (.isBefore a b) a b))

(defn extend-time-interval [interval t]
  (if-let [[l u] interval]
    [(min-instant l t) (max-instant u t)]
    (when t
      [t t])))

(defn interval-from-instants [instants]
  {:pre [(every? instant? instants)]}
  (reduce extend-time-interval nil instants))

(defn- merge-time-intervals-sub [a b]
  (cond
    (nil? a) b
    (nil? b) a
    :else (reduce extend-time-interval a b)))

(defn merge-time-intervals [& intervals]
  (reduce merge-time-intervals-sub nil intervals))

(defn duration-function
  ([^Instant reftime] (fn [] (Duration/between reftime (now))))
  ([] (duration-function (now))))

(defn estimate-progress [completed-iterations total-iterations ^Duration elapsed-duration]
  {:pre [(number? completed-iterations)
         (number? total-iterations)
         (<= completed-iterations total-iterations)
         (duration? elapsed-duration)]}
  (let [remaining-iterations (- total-iterations completed-iterations)]
    (merge {:completed-iterations completed-iterations
            :total-iterations total-iterations
            :remaining-iterations remaining-iterations}
           (if (< 0 completed-iterations)
             (let [duration-millis (.toMillis elapsed-duration)
                   duration-per-iteration (/ (double duration-millis) completed-iterations)
                   total-duration (* duration-per-iteration total-iterations)
                   remaining-duration (* duration-per-iteration remaining-iterations)
                   make-duration #(Duration/ofMillis (long %))]
               {:duration-per-iteration (make-duration duration-per-iteration)
                :remaining-duration (make-duration remaining-duration)
                :total-duration (make-duration total-duration)})
             {}))))

(defn rate-limiter [period-seconds]
  (let [period-ns (long (* 1.0e9 period-seconds))
        state (atom 0)]
    #(apply < (swap-vals!
               state
               (fn [next-notification-time]
                 (let [current-time (System/nanoTime)]
                   (if (<= next-notification-time current-time)
                     (+ current-time period-ns)
                     next-notification-time)))))))
