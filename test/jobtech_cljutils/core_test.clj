(ns jobtech-cljutils.core-test
  (:require [jobtech-cljutils.core :as core]
            [clojure.test :refer [deftest is]]))

(deftest core-test
  (is (= {:a 119} (let [a 119] (core/map-from-symbols a)))))

(defn seq-expand [x]
  (when (sequential? x)
    x))

(defn first-divider [x]
  (first (filter #(zero? (rem x %)) (range 2 x))))

(defn factor-expand [x]
  (when-let [d (first-divider x)]
    [d (quot x d)]))

(deftest flatten-with-fn-test
  (is (= [3 4] (core/flatten-with-fn seq-expand [[[3]] 4])))
  (is (= [9] (core/flatten-with-fn seq-expand 9)))
  (is (= [2 2 3 5] (core/flatten-with-fn factor-expand 60)))
  (is (= [2 2 2 2 5] (core/flatten-with-fn factor-expand 80))),
  (is (= [47] (core/flatten-with-fn factor-expand 47))))

(deftest test-find-paths
  (is (= (set (core/find-paths #(and (number? %) (odd? %)) {:a {:b [1 2 3 4 5]}}))
         #{[:a :b 4] [:a :b 2] [:a :b 0]})))


(defn add-1000-and-count
  ([] 0)
  ([state x]
   (when (number? x)
     [(inc state) (+ x 1000)])))

(defn vcount
  ([] 0)
  ([state x]
   (when (vector? x)
     [(+ state (count x)) (count x)])))

(deftest walk-and-map-test
  (is (= [9 nil] (core/walk-and-map 9 (constantly nil))))
  (is (= [[9] nil] (core/walk-and-map [9] (constantly nil))))
  (is (= [{:a 3} nil] (core/walk-and-map {:a 3} (constantly nil))))
  (is (= [{:a [1 2 3]} nil]
         (core/walk-and-map {:a [1 2 3]} (constantly nil))))
  (is (= (core/walk-and-map [1 2 3] add-1000-and-count)
         [[1001 1002 1003] 3]))
  (is (= (core/walk-and-map [:a 1 2 [[[:b 119 :c]]] 3] add-1000-and-count)
         [[:a 1001 1002 [[[:b 1119 :c]]] 1003] 4]))
  (is (= (core/walk-and-map [:a 1 2 [[[:b 119 :c]]] 3] vcount)
         [5 5]))
  (is (= (core/walk-and-map {:x [:a 1 2 [[[:b 119 :c]]] 3]
                        :y {:z {:w [4 5 6]}}} vcount)
         [{:x 5, :y {:z {:w 3}}} 8])))
