(ns jobtech-cljutils.io-test
  (:require [jobtech-cljutils.io :as io]
            [clojure.test :refer [deftest is]]
            [clojure.java.io :as jio]))

(deftest numbered-file-spitter-test
  (let [tmpdir (.toFile (io/create-temp-directory "unittest"))
        spitter (io/numbered-file-spitter (jio/file tmpdir "subdir" "out%03d.txt"))
        expected1 (jio/file tmpdir "subdir" "out001.txt")
        expected2 (jio/file tmpdir "subdir" "out002.txt")]
    (is (= expected1 (spitter "Hej")))
    (is (= expected2 (spitter "Bonjour")))
    (is (= "Hej" (slurp expected1)))
    (is (= "Bonjour" (slurp expected2)))))

