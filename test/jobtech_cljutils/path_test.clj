(ns jobtech-cljutils.path-test
  (:require [jobtech-cljutils.path :as path]
            [jobtech-cljutils.io :as jio]
            [clojure.test :refer :all]
            [clojure.java.io :as io]))


(deftest path-test
  (is (path/path? (path/path "/tmp/a.txt")))
  (is (= "/tmp/a/b/c.txt" (str (path/path "/tmp" "a" "b" "c.txt"))))
  (is (= "/tmp/a/b/c.txt" (str (path/path (path/path "/tmp") "a" "b" "c.txt")))))
