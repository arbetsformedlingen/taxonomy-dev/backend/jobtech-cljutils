(ns jobtech-cljutils.table-utils-test
  (:require [jobtech-cljutils.table-utils :as tu]
            [clojure.test :refer [deftest is]]))

(deftest various-tests
  (is (tu/nested-rows? [[1 2 3] [4 5 6]]))
  (is (= (tu/flat-table-map [[1]])
         '{[(0) (0)] 1}))
  (is (= (tu/flat-table-map [[1 2 3]])
         '{[(0) (0)] 1, [(0) (1)] 2, [(0) (2)] 3}))
  (is (= (tu/flat-table-map [[1 2 3]
                             [4 5 6]])
         '{[(0) (0)] 1,
           [(0) (1)] 2,
           [(0) (2)] 3,
           [(1) (0)] 4,
           [(1) (1)] 5,
           [(1) (2)] 6}))
  (is (= (tu/flat-table-map [[[[1 2 3]
                               [4 5 6]]]])
         '{[(0 0) (0 0)] 1,
           [(0 0) (0 1)] 2,
           [(0 0) (0 2)] 3,
           [(0 1) (0 0)] 4,
           [(0 1) (0 1)] 5,
           [(0 1) (0 2)] 6}))
  (is (= (tu/flat-table-map [[[[1]]]] str)
         '{[(0 0) (0 0)] "1"}))
  (is (= (tu/flat-nested-vectors [[[[1]]]])
         [[1]]))
  (is (= (tu/flat-nested-vectors
          [[[[[[1 2 3] [4 5 6]]]]]])
         [[1 2 3] [4 5 6]]))
  (is (= (tu/flat-nested-vectors
          (let [x [[[[1 2]]]]]
            [[x] [x] [x]]))
         [[1 2] [1 2] [1 2]]))
  (is (= (tu/flat-nested-vectors
          {[0 0] 7 [0 1] 17})
         [[7 17]]))
  (is (= (tu/flat-nested-vectors 
          [[{[0 0] 7 [0 1] 17}]
           [{[0 1] 119}]])
         [[7 17] [nil 119]]))
  (is (= (tu/flat-nested-vectors 
          [[{[0 0] 7 [0 1] 17}]
           [{[0 1] 119}]] str)
         [["7" "17"] ["" "119"]]))
  (is (= (tu/flat-nested-vectors 
          [[1 []]
           [[] 2]])
         [[1 nil] [nil 2]]))
  (is (= (tu/flat-nested-vectors [[{[0 0] "A"} {[1 0] "B"}]])
         [["A" nil] [nil "B"]]))

  (is (= [["a" "b"]] (tu/table-row "a" "b")))
  (is (= [["a"] ["b"]] (tu/table-column "a" "b")))
  (is (= [["a" "c"] ["b" "d"]]
         (tu/hcat [(tu/table-column "a" "b")
                   (tu/table-column "c" "d")])))
  (is (= [["a"] ["b"] ["c"] ["d"]]
         (tu/vcat [(tu/table-column "a" "b")
                   (tu/table-column "c" "d")]))))

(deftest test-data-matrix
  #_(is (= (tu/build-index [{:key 3} {:key 4 :children [{:key :a} {:key :b}]}])
         {[3] 0, [4] 1, [4 :a] 2, [4 :b] 3}))
  (is (= (tu/matrix {:key :a} {:key :b} {[[:a] [:b]] "hej"})
         {[0 0] "hej"})))
