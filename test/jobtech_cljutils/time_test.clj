(ns jobtech-cljutils.time-test
  (:require [jobtech-cljutils.time :as time]
            [clojure.test :refer [deftest is]])
  (:import [java.time Duration]))

(deftest instant-test
  (let [x (time/parse-instant "2023-09-20T09:30:17.563535597Z")
        y (time/parse-instant "2023-09-20T09:31:17.563535597Z")
        z (time/parse-instant "2023-09-20T09:32:17.563535597Z")]
    (is (time/instant? x))
    (is (not (time/instant? 33)))
    (is (= x (time/min-instant x y)))
    (is (= y (time/max-instant x y)))
    (is (= [y y] (time/extend-time-interval nil y)))
    (is (= [x z] (-> [y y]
                     (time/extend-time-interval z)
                     (time/extend-time-interval x))))
    (is (= [x z] (time/merge-time-intervals [x y] [y z])))
    (is (= [x z] (time/merge-time-intervals [y y] [x x] [z z])))
    (is (nil? (time/merge-time-intervals)))))

(deftest duration-test
  (is (time/duration? (Duration/ofMillis 3)))
  (is (not (time/duration? 119))))

(deftest progress-test
  (is (= (time/estimate-progress 0 3 (Duration/ofMillis 3000))
         {:completed-iterations 0,
          :total-iterations 3,
          :remaining-iterations 3}))
  (is (= (time/estimate-progress 1 3 (Duration/ofMillis 3000))
         {:completed-iterations 1,
          :total-iterations 3,
          :remaining-iterations 2,
          :duration-per-iteration (Duration/ofSeconds 3)
          :remaining-duration (Duration/ofSeconds 6)
          :total-duration (Duration/ofSeconds 9)})))
